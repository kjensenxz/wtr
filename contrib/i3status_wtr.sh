#!/bin/bash
# tacking wtr onto i3status, use as status_command in i3.conf
# https://github.com/i3/i3status/blob/master/contrib/net-speed.sh
# https://faq.i3wm.org/question/4815/adding-shell-script-to-i3statusconf.1.html

# use update interval of 1s in your config
# updates weather once, then once every 45m

_wtrcmd=~/bin/wtr                 # /path/to/wtr
_wtrloc="ICAO"                    # your ICAO
_i3conf=~/.config/i3status/config # see man i3status(1)

_i=0
_wtr=$(${_wtrcmd} ${_wtrloc})

i3status -c ${_i3conf} | \
    (read line && \
    echo "$line" && \
    read line && \
    echo "$line" && \
    read line && \
    echo "[{\"full_text\": \"${_wtr}\"},${line#,\[}" && while :
	do
		_i=$(( _i+1 ));
		# updates every 30m
		if [ $_i -gt 1800 ]; then
			_i=0
			_wtr=$(${_wtrcmd} ${_wtrloc})
		fi

		read line
		echo ",[{\"full_text\": \"${_wtr}\"},${line#,\[}" || exit 1
	done)
