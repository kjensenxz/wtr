# wtr
wtr provides a weather brief from a locality based on its ICAO.

# building
    $ make

# cleaning
    $ make clean

# usage
    $ ./wtr ICAO


# example
    # example of format for a region with windchill in effect
    $ wtr KJFK # JFK Int'l in NYC
    KJFK: Mostly Clear 32F 18MPH WSW 21F

    # example of format for a region without windchill in effect
    $ wtr PHNL # Honolulu
    PHNL: Mostly Cloudy 68F 8MPH ENW

    # example of format for a region with no wind
    $ wtr KDEN # Denver
    KDEN: Mostly Clear 8F 0MPH N


# flags
    -h: usage
    -H: usage
    -?: usage
    -v: version

# docs
- `main()` is in [`wtr.c`](wtr.c).
- main calls curl via [`web,c`](web.c), keeps its output
- sends it to [`parse.c`](parse.c).

- `nws_parse()` invokes the parsing subroutines in [`parse.c`](parse.c).
  - it uses a [`wtr_t`](parse.c#L26) struct to collect the weather information
  - it calls [`nws_preparse()`](parse.c#L58) to pre-parse the json to allocate tokens' memory.
  - it calls [jsmn](jsmn.c) to tokenize the json.
  - it calls [`nws_getinfo()`](parse.c#L138) to transform the json into a struct.
    - `nws_getinfo()` repeatedly searches for useful information in the tokens.
    - once found, it calls [`wtr_getvalue()`](parse.c#L78) to write the value into the struct.
      - `wtr_getvalue()` transforms data using definitions in [`conv.h`](conv.h).

- `main()` printf-s the struct based on if windchill is in effect or not.
- program terminates.


# licenses
wtr is licensed under the Apache License 2.0.

The scripts in the [contrib/](contrib/) directory are free and unencumbered 
software released into the public domain.

[jsmn](https://zserge.com/jsmn.html) is licensed under the MIT license.

[libcurl](https://curl.haxx.se/libcurl/) is licensed under the MIT license.
However, its dependencies may be licensed seperately, refer to 
[https://curl.haxx.se/legal/licmix.html](https://curl.haxx.se/legal/licmix.html).
