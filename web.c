/* Copyright (c) 2019 Kenneth B. Jensen <kj@0x5f3759df.xyz>
 * web.c: connect to the world wide web using libcurl
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <curl/curl.h>
#include "web.h"

char *
do_curl(const char *endpoint) {
	CURL *curl = NULL;
	CURLcode res;

	size_t size;
	char *buf = NULL;
	FILE *fp = NULL;

	fp = open_memstream(&buf, &size);
	if (fp == NULL) {
		fprintf(stderr, "Could not create dynamic memory buffer.\n");
		return NULL;
	}

	curl_global_init(CURL_GLOBAL_DEFAULT);
	curl = curl_easy_init();
	if (!curl) {
		fprintf(stderr, "Could not initialize libcurl.\n");
		return NULL;
	}

	curl_easy_setopt(curl, CURLOPT_URL, endpoint);
	/* NWS API will 403 out without a valid user agent */
	curl_easy_setopt(curl, CURLOPT_USERAGENT, "curl/7.64.0");
	/* buffer curl into buf via a FILE pointer see man open_memstream(3) */
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);

	res = curl_easy_perform(curl);
	if (res != CURLE_OK) {
		fprintf(stderr, "curl_easy_perform() failed: %s\n",
			curl_easy_strerror(res));
		fclose(fp);
		return NULL;
	}

	curl_easy_cleanup(curl);
	/* note that if the FILE ptr is not closed, buf will not contain anything. */
	fclose(fp);

	return buf;
}
