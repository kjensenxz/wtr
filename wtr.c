/* Copyright (c) 2019 Kenneth B. Jensen <kj@0x5f3759df.xyz>
 * wtr: short weather description from NWS using your local ICAO.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <errno.h>

#include "jsmn.h"
#include "web.h"
#include "conv.h"
#include "parse.h"

#ifndef VER
 #define VER 0.99.3
#endif

int
main(int argc, char **argv){
	#define URILEN 58
	const char endpoint[URILEN] = "https://api.weather.gov/stations/ICAO/observations/latest";
	char *icaoptr = strstr(endpoint, "ICAO");

	const char *arg_icao;
	char *api_res, *wtr_rpt;

	if ((argc < 2) || (argv[1][0] == '-' && 
	   (argv[1][1] == 'h' || argv[1][1] == 'H' || argv[1][1] == '?'))) {
		fprintf(stderr, "usage: %s [-v] [-h] ICAO\n", argv[0]);
		return (argc < 2) ? EXIT_FAILURE : EXIT_SUCCESS;
	}

	if (argv[1][0] == '-' && argv[1][1] == 'v') {
		fprintf(stderr, "wtr %s\n", VER);
		return EXIT_SUCCESS;
	}

	// transpose 4-letter ICAO code into endpoint
	arg_icao = argv[1];
	memcpy(icaoptr, arg_icao, 4);

	api_res = do_curl(endpoint);
	if (api_res == NULL) {
		return EXIT_FAILURE;
	}

	// XXX: daemonization
	wtr_rpt = nws_parse(api_res);
	if (wtr_rpt == NULL) {
		return EXIT_FAILURE;
	}
	printf("%s\n", wtr_rpt);
	
	free(api_res);
	free(wtr_rpt);

	return EXIT_SUCCESS;
}
