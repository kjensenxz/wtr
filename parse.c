/* Copyright (c) 2019 Kenneth B. Jensen <kj@0x5f3759df.xyz>
 * parse.c: render short info string from JSON
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "conv.h"
#include "parse.h"
#include "jsmn.h"

 //*   e.g.: "KCNM: Overcast 40F 10mph NNE 34F"
typedef struct weather {
	char *icao;         // null-terminated ICAO code
	char *desc;         // weather description
	long int tempf;     // temperature in fahrenheit
	long int wspd;      // wind speed in miles per hour
	const char *wdir;         // secondary intercardinal direction e.g. NNE
	long int wchl;      // windchill temperature in fahrenheit
} wtr_t;

/* binary mask on low order nibble
 * remember: this is for checking for value subkey
 * get "string" val: 0b0001 (e.g. json['foo'] = 'bar')
 * get "value"  val: 0b0010 (e.g. json['baz']['value'] = 'qux')
 */
typedef enum {
	WTR_ICAO = 0x01,
	WTR_DESC = 0x05, // 0x03 is 0b0011, counts as both

	WTR_WDIR = 0x02, 
	WTR_TEMP = 0x06, // 0x04 is 0b0100, counts as neither
	WTR_WSPD = 0x0A, // 0x08 is 0b1000, counts as neither

} WTR_ITEMS;

typedef enum {
	WTRMASK_STR = 0x01,
	WTRMASK_VAL = 0x02,
} WTR_MASK;

int wtr_getvalue(char *js, jsmntok_t *t, int i, WTR_ITEMS item, void *value);

/* return number of tokens to allocate, do some sanity checking */
int nws_preparse(const char *api_res, int res_len) {
	int ntok;
	jsmn_parser parser;
	jsmn_init(&parser);

	ntok = jsmn_parse(&parser, api_res, res_len, NULL, 0);
	switch (ntok) {
		case JSMN_ERROR_INVAL: // bad token
			fprintf(stderr, "failed to pre-parse bad token.\n");
			exit(EXIT_FAILURE);
		case JSMN_ERROR_PART:
		case JSMN_ERROR_NOMEM:
			fprintf(stderr, "jsmn_parse returned %d.\n"
				"please, report this at " 
				"https://gitlab.com/kjensenxz/wtr "
				"with the ICAO and time. thank you!\n",
				ntok);
			exit(EXIT_FAILURE);

	}

	return ntok;
}

int wtr_getvalue(char *js, jsmntok_t *t, int i, WTR_ITEMS item, void *value) {
	int toklen;
	char *cursor;
	double dec = NAN;

	/* check ahead; if we have a numerical value, we can then check for the
	 * "value" subkey. a key with a value already (e.g. "key": "value") 
	 * will be skipped, unless the key itself is named "value." 
	 * yes, this code is specific to the national weather service's API,
	 * though it may be usable for other geo+json (like I read the spec)
	 */
	// 0x02 == (0x03 & 0x02)
	if (WTRMASK_VAL == (item & WTRMASK_VAL)) {
		toklen = t[i+1].end - t[i+1].start;
		cursor = &js[t[i+1].start];
		if (0 == strncmp(cursor, "value", 5)) {
			toklen = t[i+2].end - t[i+2].start;
			cursor = &js[t[i+2].start];
			dec = strtod(cursor, NULL);
		}
	}

	else if (0 != (item & WTRMASK_STR)) {
		toklen = t[i].end - t[i].start;
		cursor = &js[t[i].start];
	}

	else {
		/* this should be unreachable, but if not, then leave before we
		 * end up writing garbage to &value. 
		 */
		return 1;
	}

	switch (item) {
		case WTR_TEMP:
			dec = CONV_C_TO_F(dec);
			(*(long int*)(value)) = lround(dec);
			break;
		case WTR_WSPD:
			dec = CONV_MPS_TO_MPH(dec);
			(*(long int*)(value)) = lround(dec);
			break;
		case WTR_WDIR:
			(*(const char**)(value)) = 
				degtodir(dec);
			break;
		case WTR_DESC:
			(*(char**)(value)) = 
				strndup(cursor, toklen);
			break;
		case WTR_ICAO:
			(*(char**)(value)) =
				strndup(cursor+toklen-4, 4);
			break;
	}

	return 0;
}

void nws_getinfo(char *js, jsmntok_t *t, int nt, int *i, wtr_t *w) {
	char *cursor;

	int getval = 0, item = -1;
	int itemcount = 0;
	void *value;

	#define MAXITEMS 5

	for (; *i < nt; ++*i) {
		cursor = &js[t[*i].start];

		/* check if we are getting a value for a key */
		if (1 == getval) {
			/* wtr_getvalue will load up the void pointer to the
			 * struct item with either the next token, or the next
			 * "value:" key's value in the json.
			 * i.e. "foo": "bar"
			 *      "baz": {"value": "qux"}
			 */
			if (0 == wtr_getvalue(js, t, *i, item, value)) {
				item = -1;
				getval = 0;
				++itemcount;
				/* if we have found all of the items in our 
				 * struct, we can leave this loop.
				 */
				if (MAXITEMS <= itemcount) {
					break;
				}
			}

		} 

 		/* otherwise, search for the next key */
		else if (0 == strncmp(cursor, "temperature", 11)) {
			item = WTR_TEMP;
			value = &w->tempf;
			getval = 1;
		}

		#define WLEN 4
		else if (0 == strncmp(cursor, "wind", WLEN)) {
			if (0 == strncmp(cursor+WLEN, "Speed", 5)) {
				item = WTR_WSPD;
				value = &w->wspd;
			}
			else if (0 == strncmp(cursor+WLEN, "Direction", 9)) {
				item = WTR_WDIR;
				value = &w->wdir;
			}
			else {
				continue;
			}
			getval = 1;
		}

		else if (0 == strncmp(cursor, "textDescription", 15)) {
			item = WTR_DESC;
			value = &w->desc;
			getval = 1;
		}

		else if (0 == strncmp(cursor, "station", 7)) {
			item = WTR_ICAO;
			value = &w->icao;
			getval = 1;
		}
	}

	/* only after we have the wind speed and the temperature can the
	 * windchill be calculated. so, the most reliable method is to
	 * assign after we've depleted tokens.
	 */
	w->wchl = windchill(w->tempf, w->wspd);
}


/* return a weather report from the national weather service's API.
 * format: "ICAO description temp windspeed direction windchill"
 *   e.g.: "KCNM: Overcast 40F 10mph NNE 34F"
 */
char *nws_parse(char *api_res) {
	wtr_t *wtr = NULL;
	int resln = strlen(api_res);
	int ntok;
	int i;
	char *cursor;
	jsmntok_t *tokens = NULL;; 
	jsmn_parser parser;
	char *out = NULL;

	/* a json object is at LEAST 2 characters: '{}'. */
	if (resln < 2) {
		fprintf(stderr, "Attempted to parse too short of a string.\n");
		return NULL;
	}
	
	out = calloc(256, sizeof(char));
	if (out == NULL) {
		fprintf(stderr, "Could not allocate memory for "
			"weather report string.\n");
		return NULL;
	}

	wtr = malloc(sizeof(wtr_t));
	if (wtr == NULL) {
		fprintf(stderr, "Could not allocate memory for "
			"weather object.\n");
		return NULL;
	}

	/* count number of tokens to allocate... */
	ntok = nws_preparse(api_res, resln);
	if (ntok < 0) {
		fprintf(stderr, "Could not count number of tokens "
				"to allocate.\n");
		return NULL;
	}

	/* ...and allocate them... */
	tokens = malloc(sizeof(jsmntok_t) * ntok);
	if (tokens == NULL) {
		fprintf(stderr, "Could not allocate memory for "
			"json tokens.\n");
		return NULL;
	}

	/* ...for parsing! begin! */
	jsmn_init(&parser);

	/* break json into tokens for parsing. */
	jsmn_parse(&parser, api_res, resln, tokens, ntok);

	/* find properties key; its value is the weather information object. */
	for (i = 0; i < ntok; ++i) {
		cursor = &api_res[tokens[i].start];
		
		if (tokens[i].type == JSMN_STRING &&
		    tokens[i+1].type == JSMN_OBJECT &&
		    strncmp(cursor, "properties", 10) == 0) {
			break;
		}
	}

	/* load weather information into wtr_t for formatting */
	nws_getinfo(api_res, tokens, ntok, &i, wtr);

	/* format it!
	 * windchill is only pertinent when the temperature is less than 50
	 * degrees and when the wind is blowing. 
	 */
	if (50 > wtr->tempf && 
	    0  < wtr->wspd) {
		snprintf(out, 256, "%s: %s %ldF %ldMPH %s %ldF",
			wtr->icao, wtr->desc, wtr->tempf,
			wtr->wspd, wtr->wdir, wtr->wchl);
	}
	
	/* even if the wind is 0 MPH, the direction is still important for when
	 * it will pick up, though it may shift.
	 * this may change later. your author lives in a particularly windy region.
	 */
	else {
		snprintf(out, 256, "%s: %s %ldF %ldMPH %s", wtr->icao, wtr->desc, 
			wtr->tempf, wtr->wspd, wtr->wdir);
	}
	
	free(tokens);

	free(wtr->icao);
	free(wtr->desc);
	free(wtr);

	return out;
}

