#ifndef _CONV_H_
#define _CONV_H_

/* NWS API responds with metric units. Here's some conversions. */
#define CONV_KM_TO_MI(KILOMETERS)       (KILOMETERS / 1.609344)
#define CONV_M_TO_KM(METERS)            (METERS / 1000.0)
#define CONV_M_TO_KM_TO_MI(METERS)      CONV_KM_TO_MI(CONV_M_TO_KM(METERS))
/* Just some per-time conversions */
#define CONV_MIN_TO_SEC(MINUTES)        (MINUTES * 60.0)
#define CONV_HR_TO_MIN(HOURS)           (HOURS * 60.0)
#define CONV_HR_TO_MIN_TO_SEC(HOURS)    CONV_MIN_TO_SEC(CONV_HR_TO_MIN(HOURS))

/* Finally the conversions we need */
#define CONV_MPS_TO_MPH(METERS) (CONV_M_TO_KM_TO_MI(METERS) \
                                 * CONV_HR_TO_MIN_TO_SEC(1.0))

#define CONV_C_TO_F(DEG_C) ((DEG_C * 9.0/5.0) + 32.0)

/* get windchill from temperature and wind speed */ 
int windchill(long double, long double);
/* convert degrees to secondary intercardinal directions */
const char *degtodir(double);

#endif /* _CONV_H_ */
