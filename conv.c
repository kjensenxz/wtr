/* Copyright (c) 2019 Kenneth B. Jensen <kj@0x5f3759df.xyz>
 * conv.c: some tables, functions, & macros for NWS API conversions
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <math.h>
#include "conv.h"

/* T -> temperature in fahrenheit
 * V -> wind speed in mph
 *
 * formula from https://www.weather.gov/media/epz/wxcalc/windChill.pdf
 *  V′ = pow(V, 0.16)
 * °F  = {T|T≤50°F} 35.74 + (0.6215 * T) - (35.75 * V') + ((0.4275 * T) * V'))
 */
int
windchill(long double T, long double V) {
	long double vpow, wc;
	
	vpow = powl(V, 0.16);
	wc = 35.74 + (0.6215 * T) - (35.75 * vpow) + ((0.4275 * T) * vpow);
	
	return (int) roundl(wc);
}

/*     degree          deg/11.25
 *    interval         interval      direction
 * [  0.00,11.25]       [ 0, 1]         N
 * ( 11.25,33.75]       ( 1, 3]         NNE
 * ( 33.75,56.25]       ( 3, 5]         NE
 * ( 56.25,78.75]       ( 5, 7]         ENW
 * ( 78.75,101.25]      ( 7, 9]         E
 * (101.25,123.75]      ( 9,11]         ESE
 * (123.75,146.25]      (11,13]         SE
 * (146.25,168.75]      (13,15]         SSE
 * (168.75,191.25]      (15,17]         S
 * (191.25,213.75]      (17,19]         SSW
 * (213.75,236.25]      (19,21]         SW 
 * (236.25,258.75]      (21,23]         WSW
 * (258.75,281.25]      (23,25]         W
 * (281.25,303.75]      (25,27]         WNW
 * (303.75,326.25]      (27,29]         NW
 * (326.25,348.75]      (29,31]         NNW
 * (348.75,360.00]      (31,32]         N
 */
const char *
degtodir(double angle) {
	double direction = angle / 11.25;
	
	if (direction >  1 && direction <=  3) 
		return "NNE";
	if (direction >  3 && direction <=  5) 
		return "NE";
	if (direction >  5 && direction <=  7) 
		return "ENW";
	if (direction >  7 && direction <=  9) 
		return "E";
	if (direction >  9 && direction <= 11) 
		return "ESE";
	if (direction > 11 && direction <= 13) 
		return "SE";
	if (direction > 13 && direction <= 15) 
		return "SSE";
	if (direction > 15 && direction <= 17) 
		return "S";
	if (direction > 17 && direction <= 19) 
		return "SSW";
	if (direction > 19 && direction <= 21) 
		return "SW";
	if (direction > 21 && direction <= 23) 
		return "WSW";
	if (direction > 23 && direction <= 25) 
		return "W";
	if (direction > 25 && direction <= 27) 
		return "WNW";
	if (direction > 27 && direction <= 29) 
		return "NW";
	if (direction > 29 && direction <= 31) 
		return "NNW";

	return "N";
}
