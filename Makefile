VER='"0.99.3"'

CC = gcc
SRC = *.c
BIN = wtr
LDLIBS = -lcurl -lm
CFLAGS += -Wall -Wpedantic -Wextra -std=gnu11 -O3
PREFIX = ~/bin/

all:
	$(CC) $(LDLIBS) $(CFLAGS) $(SRC) -D VER=$(VER) -o $(BIN)

clean:
	rm -f *.o $(BIN)

install:
	cp $(BIN) $(PREFIX)$(BIN)
